cmake_minimum_required(VERSION 3.0.0) 

# Generate a project ID from the containing directory name.
get_filename_component(ProjectId ${CMAKE_CURRENT_LIST_DIR} NAME)
project(${ProjectId} C CXX)

# Locate and configure the Google Protocol Buffer library.
INCLUDE(FindProtobuf)
FIND_PACKAGE(Protobuf REQUIRED)

# Source files contained in this target.
set(COMMON_SRCS ${CMAKE_CURRENT_SOURCE_DIR}/measurePayload.proto
                ${CMAKE_CURRENT_SOURCE_DIR}/keyboardPayload.proto
                ${CMAKE_CURRENT_SOURCE_DIR}/hw_evt.proto
                ${CMAKE_CURRENT_SOURCE_DIR}/clockPayload.proto)

# Process the .proto files into C++.
protobuf_generate_cpp(PROTO_SRC PROTO_HEADER ${COMMON_SRCS})

# Primary target of this build.
add_library(${ProjectId} STATIC ${PROTO_HEADER} ${PROTO_SRC})
target_link_libraries(${ProjectId} ${PROTOBUF_LIBRARY})

# Configure so any target which depends on this library automatically has the appropriate include directories added to 
# its own, so explicit call to include_directories is not required.
target_include_directories(${ProjectId} PUBLIC ${CMAKE_CURRENT_BINARY_DIR})

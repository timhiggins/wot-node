/*eslint-disable block-scoped-var, no-redeclare, no-control-regex, no-prototype-builtins*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.proto_lib = (function() {

    /**
     * Namespace proto_lib.
     * @exports proto_lib
     * @namespace
     */
    var proto_lib = {};

    proto_lib.HwEvt = (function() {

        /**
         * Properties of a HwEvt.
         * @memberof proto_lib
         * @interface IHwEvt
         * @property {proto_lib.HwEvt.EvtType|null} [evtType] HwEvt evtType
         * @property {proto_lib.HwEvt.EvtModifier|null} [evtModifier] HwEvt evtModifier
         * @property {number|null} [value] HwEvt value
         */

        /**
         * Constructs a new HwEvt.
         * @memberof proto_lib
         * @classdesc Represents a HwEvt.
         * @implements IHwEvt
         * @constructor
         * @param {proto_lib.IHwEvt=} [properties] Properties to set
         */
        function HwEvt(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * HwEvt evtType.
         * @member {proto_lib.HwEvt.EvtType} evtType
         * @memberof proto_lib.HwEvt
         * @instance
         */
        HwEvt.prototype.evtType = 0;

        /**
         * HwEvt evtModifier.
         * @member {proto_lib.HwEvt.EvtModifier} evtModifier
         * @memberof proto_lib.HwEvt
         * @instance
         */
        HwEvt.prototype.evtModifier = 0;

        /**
         * HwEvt value.
         * @member {number} value
         * @memberof proto_lib.HwEvt
         * @instance
         */
        HwEvt.prototype.value = 0;

        /**
         * Creates a new HwEvt instance using the specified properties.
         * @function create
         * @memberof proto_lib.HwEvt
         * @static
         * @param {proto_lib.IHwEvt=} [properties] Properties to set
         * @returns {proto_lib.HwEvt} HwEvt instance
         */
        HwEvt.create = function create(properties) {
            return new HwEvt(properties);
        };

        /**
         * Encodes the specified HwEvt message. Does not implicitly {@link proto_lib.HwEvt.verify|verify} messages.
         * @function encode
         * @memberof proto_lib.HwEvt
         * @static
         * @param {proto_lib.IHwEvt} message HwEvt message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        HwEvt.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.evtType != null && message.hasOwnProperty("evtType"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.evtType);
            if (message.evtModifier != null && message.hasOwnProperty("evtModifier"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.evtModifier);
            if (message.value != null && message.hasOwnProperty("value"))
                writer.uint32(/* id 3, wireType 0 =*/24).uint32(message.value);
            return writer;
        };

        /**
         * Encodes the specified HwEvt message, length delimited. Does not implicitly {@link proto_lib.HwEvt.verify|verify} messages.
         * @function encodeDelimited
         * @memberof proto_lib.HwEvt
         * @static
         * @param {proto_lib.IHwEvt} message HwEvt message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        HwEvt.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a HwEvt message from the specified reader or buffer.
         * @function decode
         * @memberof proto_lib.HwEvt
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {proto_lib.HwEvt} HwEvt
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        HwEvt.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.proto_lib.HwEvt();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.evtType = reader.int32();
                    break;
                case 2:
                    message.evtModifier = reader.int32();
                    break;
                case 3:
                    message.value = reader.uint32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a HwEvt message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof proto_lib.HwEvt
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {proto_lib.HwEvt} HwEvt
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        HwEvt.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a HwEvt message.
         * @function verify
         * @memberof proto_lib.HwEvt
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        HwEvt.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.evtType != null && message.hasOwnProperty("evtType"))
                switch (message.evtType) {
                default:
                    return "evtType: enum value expected";
                case 0:
                    break;
                }
            if (message.evtModifier != null && message.hasOwnProperty("evtModifier"))
                switch (message.evtModifier) {
                default:
                    return "evtModifier: enum value expected";
                case 0:
                case 1:
                    break;
                }
            if (message.value != null && message.hasOwnProperty("value"))
                if (!$util.isInteger(message.value))
                    return "value: integer expected";
            return null;
        };

        /**
         * Creates a HwEvt message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof proto_lib.HwEvt
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {proto_lib.HwEvt} HwEvt
         */
        HwEvt.fromObject = function fromObject(object) {
            if (object instanceof $root.proto_lib.HwEvt)
                return object;
            var message = new $root.proto_lib.HwEvt();
            switch (object.evtType) {
            case "EvtTypeProbeDet":
            case 0:
                message.evtType = 0;
                break;
            }
            switch (object.evtModifier) {
            case "EvtModifierOn":
            case 0:
                message.evtModifier = 0;
                break;
            case "EvtModifierOff":
            case 1:
                message.evtModifier = 1;
                break;
            }
            if (object.value != null)
                message.value = object.value >>> 0;
            return message;
        };

        /**
         * Creates a plain object from a HwEvt message. Also converts values to other types if specified.
         * @function toObject
         * @memberof proto_lib.HwEvt
         * @static
         * @param {proto_lib.HwEvt} message HwEvt
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        HwEvt.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.evtType = options.enums === String ? "EvtTypeProbeDet" : 0;
                object.evtModifier = options.enums === String ? "EvtModifierOn" : 0;
                object.value = 0;
            }
            if (message.evtType != null && message.hasOwnProperty("evtType"))
                object.evtType = options.enums === String ? $root.proto_lib.HwEvt.EvtType[message.evtType] : message.evtType;
            if (message.evtModifier != null && message.hasOwnProperty("evtModifier"))
                object.evtModifier = options.enums === String ? $root.proto_lib.HwEvt.EvtModifier[message.evtModifier] : message.evtModifier;
            if (message.value != null && message.hasOwnProperty("value"))
                object.value = message.value;
            return object;
        };

        /**
         * Converts this HwEvt to JSON.
         * @function toJSON
         * @memberof proto_lib.HwEvt
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        HwEvt.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * EvtType enum.
         * @name proto_lib.HwEvt.EvtType
         * @enum {string}
         * @property {number} EvtTypeProbeDet=0 EvtTypeProbeDet value
         */
        HwEvt.EvtType = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "EvtTypeProbeDet"] = 0;
            return values;
        })();

        /**
         * EvtModifier enum.
         * @name proto_lib.HwEvt.EvtModifier
         * @enum {string}
         * @property {number} EvtModifierOn=0 EvtModifierOn value
         * @property {number} EvtModifierOff=1 EvtModifierOff value
         */
        HwEvt.EvtModifier = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "EvtModifierOn"] = 0;
            values[valuesById[1] = "EvtModifierOff"] = 1;
            return values;
        })();

        return HwEvt;
    })();

    return proto_lib;
})();

module.exports = $root;

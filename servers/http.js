// Requires the express framework, routes, and the model.
var express = require('express'),
    actuatorsRoutes = require('./../routes/actuators'),
    sensorRoutes = require('./../routes/sensors'),
    resources = require('./../resources/model'),
    cors = require('cors');

// Creates an application with the express framework; the wraps the http server.
var app = express();

// Enable CORS: to enable client-side javascript to access resources.
app.use(cors());

// Binds routes to the Express application.
app.use('/pi/actuators', actuatorsRoutes);
app.use('/pi/sensors', sensorRoutes);

// Creates a default route for /pi
app.get('/pi', function(req, res) {
    res.send('This is the WoT-Pi!')
});

module.exports = app;

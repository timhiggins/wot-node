// Require and instantiatei and express router to define the path to our resources.
var express = require('express'), router = express.Router(), resources = require('./../resources/model');

// Create a new route for a GET request on all actuators and attach a callback function.
router.route('/').get(function(req, res, next) {
    // Reply with the actuators model when this route is selected.
    req.result = resources.pi.actuators;
    next();
});

// Create a new route for a GET request on all leds and attach a callback function.
router.route('/leds').get(function(req, res, next) {
    // Reply with the leds model when this route is selected.
    req.result = resources.pi.actuators.leds;
    next();
});

// Create a new route for a GET request for a specific LED and attach a callback function.
router.route('/leds/:id').get(function(req, res, next) { 
    req.result = resources.pi.actuators.leds[req.params.id];
    next();
// Callback for a PUT request on an LED.
}).put(function(req, res, next) { 
    var selectedLed = resources.pi.actuators.leds[req.params.id];

    // Update the value of the selected LED in the model.
    selectedLed.value = req.body.value;
    req.result = selectedLed;
    next();
});

// Export router to make it accessible for 'requires' of this file.
module.exports = router;


// Require and instantiatei and express router to define the path to our resources.
var express = require('express'), router = express.Router(), resources = require('./../resources/model');

// Create a new route for a GET request on all sensors and attach a callback function.
router.route('/').get(function(req, res, next) {
    // Assign the results, the sensors model, to a new property of the req object that you pass along from middleware 
    // to middleware.
    req.result = resources.pi.sensors; 

    // Call the next middleware; the framework will ensure the next middleware gets access to req (including the 
    // req.result) and res.
    next(); 
});

// Create a new route for a GET request on PIR and attach a callback function.
router.route('/pir').get(function(req, res, next) {
    req.result = resources.pi.sensors.pir;
    next();
});

// Create a new route for a GET request on temperature and attach a callback function.
router.route('/temperature').get(function(req, res, next) {
    req.result = resources.pi.sensors.temperature;
    next();
});

// Create a new route for a GET request on humidity and attach a callback function.
router.route('/humidity').get(function(req, res, next) {
    req.result = resources.pi.sensors.humidity;
    next();
});

// Export router to make it accessible for 'requires' of this file.
module.exports = router;


// Load the HTTP server and the model.
var httpServer = require('./servers/http'),
    resources = require('./resources/model');

// Start the HTTP server by invoking listen() on the Express application.
var server = httpServer.listen(resources.pi.port, function () {
    // Once the server is started, the callback is invoked. 
    console.log('HTTP server started...');
    console.info('Your WoT Pi is up and running on port %s', resources.pi.port);
});


var zmq = require('zeromq')
var subscriber = zmq.socket('sub')
var protobuf = require("protobufjs");

subscriber.on('message', function() {
    var msg = [];
    Array.prototype.slice.call(arguments).forEach(function(arg) { 
        msg.push(arg);
    });

    switch(msg[0].toString()) {
        case "time":
            processClockPayload(msg);
            break;
        default:
            console.log("default");
            break;
    }
})

subscriber.connect('tcp://localhost:5556')
subscriber.subscribe('time')

function processClockPayload(evt) {
    protobuf.load("/opt/hachdev/project/r1701/r1701_node/proto/clockPayload.proto", function(err, root) {
        if(err)
            throw err;
        
        // Obtain payload type.
        var clockPayload = root.lookupType("proto_lib.Clock");

        // Decode event payload.
        var message = clockPayload.decode(evt[1]);
        var object = clockPayload.toObject(message, {
            enums: String,  // enums as string names
            defaults: true, // includes default values
        });

        // Print key-value pairs of plain old object.
        /*
        Object.keys(object).forEach(function(key) {
            console.log(key, object[key]);
        });
        */

        var localDate = new Date(message.timestamp * 1000);
        console.log("time_evt: " + object["timeEvt"] + ", timestamp: " + localDate);
    });
}

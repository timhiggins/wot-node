var zmq = require('zeromq')
var publisher = zmq.socket('pub')
var protobuf = require("protobufjs");

publisher.connect('tcp://localhost:5555');

setInterval(function() {

    console.log("Here.");
    protobuf.load("/opt/hachdev/project/r1701/r1701_node/proto/hw_evt.proto", function(err, root) {
        if(err)
            throw err;
        
        // Obtain payload type.
        var hw_evt = root.lookupType("proto_lib.HwEvt");

        // Populate payload content.
        var payload_data = { evtType: 0,
                             evtModifier: 1,
                             value: 9
        };

        // Verify the payload.
        var errMsg = hw_evt.verify(payload_data);
        if(errMsg)
            throw Error(errMsg);

        // Create the payload.
        var message = hw_evt.create(payload_data);
        var buffer = hw_evt.encode(message).finish();

        //publisher.send(["hw_evt", payload]);
        publisher.send("hw_evt", zmq.ZMQ_SNDMORE);
        publisher.send(buffer);
    });

},1000);

